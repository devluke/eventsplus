package net.lukepchambers.minecraft.eventsplus.events

import net.lukepchambers.minecraft.eventsplus.EventsPlus

class BuildBattleEvent(plugin: EventsPlus): EventsPlusEvent(plugin) {

    override val minPlayers = 2

    override fun initEvent() {}
    override fun startEvent() {}
    override fun endEvent() {}

    override fun determineWinner() = ""

}