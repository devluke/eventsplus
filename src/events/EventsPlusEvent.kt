package net.lukepchambers.minecraft.eventsplus.events

import net.lukepchambers.minecraft.eventsplus.EventsPlus

abstract class EventsPlusEvent(protected val plugin: EventsPlus) {

    protected abstract val minPlayers: Int

    protected abstract fun initEvent()
    protected abstract fun startEvent()
    protected abstract fun endEvent()

    protected abstract fun determineWinner(): String

}