package net.lukepchambers.minecraft.eventsplus

import fr.rhaz.minecraft.kotlin.bukkit.*
import fr.rhaz.minecraft.kotlin.textOf
import net.lukepchambers.minecraft.eventsplus.events.BuildBattleEvent
import net.lukepchambers.minecraft.eventsplus.events.EventsPlusEvent
import net.md_5.bungee.api.chat.TextComponent
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerQuitEvent

class EventsPlus: BukkitPlugin() {

    private lateinit var plugin: EventsPlus

    private val events = mutableMapOf<String, EventsPlusEvent>()

    private val console = server.consoleSender!!

    private val cmds = listOf("help", "start", "end", "join", "leave", "info", "kick", "broadcast", "reload")

    object Config: ConfigFile("config") {

        var pluginVersion by string("version.plugin")
        var configVersion by string("version.config")

        var displayAllMessages by boolean("messages.display-all-messages")
        var enablePrefix by boolean("messages.enable-prefix")

    }

    object Data: ConfigFile("data") {

        var event by boolean("event.event")
        var started by boolean("event.started")

        var owner by string("event.owner")
        var players by stringList("event.players")

        var type by string("event.type")
        var time by int("event.time")

    }

    object Messages: ConfigFile("messages") {

        var prefix by string("global.prefix")

    }

    override fun onEnable() {

        plugin = this

        endEvent()

        // update(12345, COLOR, "permission")

        init(Config)
        init(Data)
        init(Messages)

        registerCustomEvent("build-battle", "EventsPlus", BuildBattleEvent(plugin))

        listen<PlayerQuitEvent> {

            if (it.player.isOwner()) endEvent()

        }

        console.fmsg("Plugin enabled.")

    }

    override fun onDisable() {

        endEvent()

        console.fmsg("Plugin disabled.")

    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {

        val isPlayer = sender is Player
        fun mustBePlayer(): Boolean {

            sender.fmsg("You must be a player to do that.")

            return true

        }

        val isOwner = if (isPlayer) (sender as Player).isOwner() else true
        fun mustBeOwner(): Boolean {

            sender.fmsg("You must be the event owner to do that.")

            return true

        }

        val inEvent = if (isPlayer) (sender as Player).inEvent() else false
        fun mustBeInEvent(): Boolean {

            sender.fmsg("You must be in the event to do that.")

            return true

        }

        val isEvent = Data.event
        fun noEvent(): Boolean {

            sender.fmsg("There is currently no event.")

            if (sender.has("cmd.start")) {

                sender.fmsg("Start one with /$label start.")

            }

            return true

        }

        fun alreadyInEvent(): Boolean {

            sender.fmsg("You are already in the event.")

            return true

        }

        fun noPerm(perm: String): Boolean {

            sender.fmsg("You don't have permission to do that.")
            sender.fmsg("eventsplus.$perm")

            return true

        }

        fun invalidArgs(usage: String? = null): Boolean {

            sender.fmsg("Invalid arguments.")
            sender.fmsg("Usage: /$label${if (usage == null) " " else ""}$usage")

            return true

        }

        fun help(): Boolean {

            sender.fmsg("EventsPlus v${description.version}:")

            cmds.forEach { sender.fmsg("/$label $it") }

            return true

        }

        if (args.isNotEmpty() && args[0] !in cmds) {

            sender.fmsg("Invalid subcommand.")
            sender.fmsg("Use /$label help for a list of subcommands.")

            return true

        }

        if (args.isEmpty()) help()

        val perm = "cmd." + if (args.isEmpty()) "help" else args[0]

        if (args.isNotEmpty() && !sender.has(perm)) {

            return noPerm("eventsplus.$perm")

        }

        val subArgs = args.toList().subList(1, args.size - 1)

        val pl = (if (isPlayer) sender as Player else null)!!

        when (args.elementAtOrNull(0)) {

            "help" -> { // 2

                if (subArgs.isNotEmpty()) return invalidArgs()

                help()

                return true

            }

            "start" -> { // 1

                if (!isPlayer) return mustBePlayer()
                if (subArgs.size !in 1..2) return invalidArgs("<type> [minutes]")

                var time = 5

                if ((subArgs.size == 2) && (subArgs[1].toIntOrNull() != null)) time = subArgs[1].toInt()

                // TODO: Validate event

                Data.event = true
                Data.started = false

                Data.owner = pl.name
                Data.players = listOf()

                Data.type = subArgs[0]

                // TODO: Teleport players

                return true

            }

            "end" -> { // 2

                if (!isOwner) return mustBeOwner()
                if (subArgs.isNotEmpty()) return invalidArgs()

                endEvent()

                sender.fmsg("Successfully ended event.")

                return true

            }

            "join" -> { // 1

                if (!isPlayer) return mustBePlayer()
                if (inEvent) return alreadyInEvent()
                if (subArgs.isNotEmpty()) return invalidArgs()

                return true

            }

            "leave" -> { // 2

                if (!isPlayer) return mustBePlayer()
                if (!inEvent) return mustBeInEvent()
                if (subArgs.isNotEmpty()) return invalidArgs()

                pl.leaveEvent()

                pl.fmsg("You have left the event.")

                bc("${pl.name} has left the event.", false)

                return true

            }

            "info" -> { // 1

                if (!isEvent) return noEvent()
                if (subArgs.isNotEmpty()) return invalidArgs()

                return true

            }

            "kick" -> { // 2

                if (!isOwner) return mustBeOwner()
                if (subArgs.isEmpty()) return invalidArgs("<player> [reason]")

                val kick = server.getPlayer(subArgs[0])

                if (kick == null) {

                    sender.fmsg("That player can't be found.")
                    sender.fmsg("Make sure they're online and visible.")

                    return true

                }

                if (kick.name !in Data.players) {

                    sender.fmsg("That player must be in the event to do that.")

                    return true

                }

                kick.leaveEvent()

                kick.fmsg("You've been kicked from the event.")

                if (subArgs.size >= 2) {

                    kick.fmsg("Reason: ${subArgs.subList(1, subArgs.size - 1).joinToString(" ")}")

                }

                sender.fmsg("Successfully kicked ${kick.name}.")

                bc("${kick.name} has been kicked from the event.", false)

                return true

            }

            "broadcast" -> { // 2

                if (!isOwner) return mustBeOwner()
                if (subArgs.isEmpty()) return invalidArgs()

                bc(subArgs.joinToString(" "), false)

                return true

            }

            "reload" -> { // 2

                if (subArgs.isNotEmpty()) return invalidArgs()

                endEvent()

                init(Config)
                init(Data)
                init(Messages)

                sender.fmsg("EventsPlus v${description.version} reloaded.")

                return true

            }

        }

        return false

    }

    fun endEvent(winner: String? = null) {

        if (!Data.event) return

        Data.players.forEach {

            val pl = server.getPlayer(it)

            pl.leaveEvent()

        }

        Data.event = false
        Data.started = false

        Data.owner = ""
        Data.players = listOf()

        Data.type = ""
        Data.time = 0

        var msg = "The event is over"

        msg += if (winner == null) ", there is no winner!" else ", $winner has won!"

        bc(msg, true)

    }

    fun msg(msg: String): TextComponent {

        val prefix = if (Config.enablePrefix) Messages.prefix else ""

        // val message = Config["messages.$msg"]

        return textOf("$prefix$msg")

    }

    fun bc(msg: String, important: Boolean) {

        if (important || Config.displayAllMessages) server.broadcastMessage(msg(msg).text)
        else {

            Data.players.forEach {

                val pl = server.getPlayer(it)

                pl.fmsg(msg(msg).text)

            }

        }

    }

    fun registerCustomEvent(name: String, pluginName: String, event: EventsPlusEvent) {

        val eventName = "$pluginName$name"

        events[eventName] = event

    }

    fun BukkitSender.fmsg(msg: String) {

        msg(plugin.msg(msg))

    }

    fun BukkitSender.has(perm: String) = hasPermission("eventsplus.$perm")

    fun Player.isOwner() = (name == Data.owner) || has("staff")

    fun Player.inEvent() = name in Data.players

    fun Player.leaveEvent() {

        Data.players = Data.players - name

        teleport(world.spawnLocation)

    }

}