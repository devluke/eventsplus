# EventsPlus

Spigot plugin that adds events to your server

## Creating events

EventsPlus provides an easy way to create your own events.

First, you need to import EventsPlus into your plugin.
You can do this by downloading the JAR file found on the plugin's SpigotMC page.
Maven/Gradle is coming soon.

---

You then need to create a class for your event.
All code here is in Kotlin, the programming language the plugin was coded in.

```kotlin
class BuildBattleEvent(plugin: EventsPlus): EventsPlusEvent(plugin) {

    // The minimum amount of players your event can have
    override val minPlayers = 2

    override fun initEvent() {
    
        // Code to be ran when the event is first created, before it's started
    
    }
    
    override fun startEvent() {
    
        // Code to be ran right after the event starts
    
    }
    
    override fun endEvent() {
    
        // Code to be ran right after the event ends
    
    }
    
    override fun determineWinner(): Player {
    
        // Code used to determine the winner
        
        return winner
    
    }
    
}
```

---

Finally, you need to register your event in `onEnable()`.

```kotlin
override fun onEnable() {

    registerEvent("build-battle", "EventsPlus", BuildBattleEvent(plugin))
    
}
```

The first parameter is the name of your event.
Typically it's named in lisp-case.

The second parameter is the name of your plugin as it is in your plugin YAML file.

The final parameter is an instance of your event class with an instance of your plugin as its only parameter.
If you don't have a plugin variable, just use `this`.